.. |date| date::  %A %d %B %Y
.. |time| date::  %H:%M:%S

.. include:: README.rst


CI Output Results
-----------------

* `Documentation <doc>`_
* `Coverage <coverage>`_
* `Unit tests <pytest_report.html>`_
* `Pylint <pylint_report.html>`_


.. footer:: This document was generated on |date| at |time|.

Copyright (c) 2017 - Hans Smit
